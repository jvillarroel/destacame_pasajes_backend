"""pasajes URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from rest_framework import routers
from apps.bus import views as viewsBus
from apps.passenger import views as viewsPassenger
from apps.journey import views as viewsJourney
from apps.ticket import views as viewsTicket
from rest_framework.authtoken import views

router = routers.DefaultRouter()
router.register(r'passengers', viewsPassenger.PassengerViewSet)
router.register(r'drivers', viewsBus.DriverViewSet)
router.register(r'cities', viewsJourney.CityViewSet)
router.register(r'bus', viewsBus.BusViewSet)
router.register(r'journeys', viewsJourney.JourneyViewSet)
router.register(r'schedules', viewsJourney.ScheduleViewSet, 'Schedules')
router.register(r'seats', viewsBus.SeatViewSet, 'Seats')
router.register(r'tickets', viewsTicket.TicketViewSet)


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url('', include(router.urls)),
    url('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^api-token-auth/', views.obtain_auth_token)
]
