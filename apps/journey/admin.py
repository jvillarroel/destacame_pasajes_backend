# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

# Register your models here.
from .models import City, Schedule, Journey

admin.site.register(City)
admin.site.register(Schedule)
admin.site.register(Journey)