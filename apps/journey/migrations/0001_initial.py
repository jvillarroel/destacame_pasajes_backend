# -*- coding: utf-8 -*-
# Generated by Django 1.11.20 on 2019-04-02 01:59
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('bus', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='City',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255, unique=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.CreateModel(
            name='Journey',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('end', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='city_end', to='journey.City')),
                ('start', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='city_start', to='journey.City')),
            ],
        ),
        migrations.CreateModel(
            name='Schedule',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('time_start', models.DateTimeField(blank=True)),
                ('time_end', models.DateTimeField(blank=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('bus', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='schedule_bus', to='bus.Bus')),
                ('driver', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='schedule_driver', to='bus.Driver')),
                ('journey', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='schedule_journey', to='journey.Journey')),
            ],
        ),
    ]
