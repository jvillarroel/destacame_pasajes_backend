# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response
from django.db.models import Count, FloatField, Subquery, ExpressionWrapper
from django.db.models.functions import Cast
from .models import Journey, City, Schedule
from .serializers import JourneySerializer, JourneyAverageSerializer, \
    CitySerializer, ScheduleSerializer, ScheduleCapacitySerializer

# Create your views here.
class JourneyViewSet(viewsets.ModelViewSet):
    queryset = Journey.objects.all()
    serializer_class = JourneySerializer

    @action(detail=False)
    def average(self, request):
        queryset = Journey.objects.all()
        serializer = JourneyAverageSerializer(queryset, many=True)
        return Response(serializer.data)

class CityViewSet(viewsets.ModelViewSet):
    queryset = City.objects.all()
    serializer_class = CitySerializer

class ScheduleViewSet(viewsets.ModelViewSet):
    serializer_class = ScheduleSerializer

    def get_queryset(self):
        queryset = Schedule.objects.all()
        start = self.request.query_params.get('start', None)
        end = self.request.query_params.get('end', None)
        if start is not None:
            queryset = queryset.filter(journey__start=start)
        if end is not None:
            queryset = queryset.filter(journey__end=end)
        return queryset

    @action(detail=False)
    def capacity(self, request):
        journey = self.request.query_params.get('journey', None)
        percentage = self.request.query_params.get('percentage', None)
        queryset = Schedule.objects.all()
        if journey is not None and percentage is not None:
            count_sq = Schedule.objects.annotate(total=Cast(Count('bus__seat_bus'), FloatField()))\
                .filter(journey=journey)
            queryset = Schedule.objects.annotate(
                percentage=ExpressionWrapper(
                    Count('ticket_schedule') * 100 / Subquery(count_sq.values('total')), output_field=FloatField()))\
                .filter(percentage__gte=float(percentage), journey=journey)
        serializer = ScheduleCapacitySerializer(queryset, many=True)
        return Response(serializer.data)