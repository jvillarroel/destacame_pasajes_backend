from .models import Journey, City, Schedule
from rest_framework import serializers
from django.db.models import Avg, Count

class CitySerializer(serializers.ModelSerializer):
    class Meta:
        model = City
        fields = ('id', 'name')

class JourneySerializer(serializers.ModelSerializer):
    fullName = serializers.ReadOnlyField()
    city_start = serializers.CharField(read_only=True, source="start.name")
    city_end = serializers.CharField(read_only=True, source="end.name")
    class Meta:
        model = Journey
        fields = ('id', 'start', 'end', 'city_start', 'city_end', 'fullName')

class JourneyAverageSerializer(serializers.ModelSerializer):
    num_schedules = serializers.SerializerMethodField()
    num_passengers = serializers.SerializerMethodField()
    average_passenger = serializers.SerializerMethodField()
    average_real = serializers.SerializerMethodField()
    city_start = serializers.CharField(read_only=True, source="start.name")
    city_end = serializers.CharField(read_only=True, source="end.name")
    class Meta:
        model = Journey
        fields = ('id', 'start', 'end', 'city_start', 'city_end',
                  'num_passengers', 'num_schedules', 'average_passenger', 'average_real')

    def get_num_schedules(self, obj):
        return obj.schedule_journey.all().count()

    def get_num_passengers(self, obj):
        return obj.schedule_journey.all().aggregate(num_tickets=Count('ticket_schedule')).get('num_tickets')

    def get_average_passenger(self, obj):
        average = obj.schedule_journey.annotate(num_tickets=Count('ticket_schedule'))\
            .aggregate(Avg('num_tickets')).get('num_tickets__avg')
        if average is None:
            return 0
        return average

    def get_average_real(self, obj):
        schedules = obj.schedule_journey.all().count()
        if(schedules == 0):
            schedules = 1
        passengers = obj.schedule_journey.all().aggregate(n=Count('ticket_schedule')).get('n')
        average = float(passengers) / float(schedules)
        return average

class ScheduleSerializer(serializers.ModelSerializer):
    city_start = serializers.CharField(read_only=True, source="journey.start.name")
    city_end = serializers.CharField(read_only=True, source="journey.end.name")
    bus_registration = serializers.CharField(read_only=True, source="bus.registration")
    class Meta:
        model = Schedule
        fields = ('id', 'time_start', 'time_end', 'driver', 'journey', 'bus', 'city_start', 'city_end', 'bus_registration')

class ScheduleCapacitySerializer(serializers.ModelSerializer):
    city_start = serializers.CharField(read_only=True, source="journey.start.name")
    city_end = serializers.CharField(read_only=True, source="journey.end.name")
    bus_registration = serializers.CharField(read_only=True, source="bus.registration")
    number_bus_seats = serializers.SerializerMethodField()
    number_ocuppied_seats = serializers.SerializerMethodField()
    ocuppied_percentage = serializers.SerializerMethodField()

    class Meta:
        model = Schedule
        fields = ('id', 'time_start', 'time_end', 'driver', 'journey', 'bus', 'city_start',
                  'city_end', 'bus_registration', 'number_bus_seats', 'number_ocuppied_seats',
                  'ocuppied_percentage')

    def get_ocuppied_percentage(self, obj):
        return obj.percentage

    def get_number_bus_seats(self, obj):
        return obj.bus.seat_bus.count()

    def get_number_ocuppied_seats(self, obj):
        return obj.ticket_schedule.count()