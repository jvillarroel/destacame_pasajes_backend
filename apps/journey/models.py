# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class City(models.Model):
    name = models.CharField(max_length=255, unique=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

class Journey(models.Model):
    start = models.ForeignKey(City, related_name='city_start')
    end = models.ForeignKey(City, related_name='city_end')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def fullName(self):
        return self.start.name + " - " + self.end.name

class Schedule(models.Model):
    time_start = models.DateTimeField(null=False, blank=True)
    time_end = models.DateTimeField(null=False, blank=True)
    driver = models.ForeignKey('bus.Driver', related_name='schedule_driver', null=True, on_delete=models.CASCADE)
    journey = models.ForeignKey(Journey, related_name='schedule_journey', on_delete=models.CASCADE)
    bus = models.ForeignKey('bus.Bus', related_name='schedule_bus', null=True, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)