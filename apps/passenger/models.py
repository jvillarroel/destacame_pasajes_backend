# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class Passenger(models.Model):
    name = models.CharField(max_length=255)
    lastname = models.CharField(max_length=255)
    age = models.IntegerField()
    gender_options = (
        ('h', 'Hombre'),
        ('m', 'Mujer'),
    )
    gender = models.CharField(max_length=1, choices=gender_options, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def fullName(self):
        return self.name + " " + self.lastname