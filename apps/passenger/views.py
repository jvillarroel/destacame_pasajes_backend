# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from .models import Passenger
from rest_framework import viewsets
from .serializers import PassengerSerializer

class PassengerViewSet(viewsets.ModelViewSet):
    queryset = Passenger.objects.all()
    serializer_class = PassengerSerializer
