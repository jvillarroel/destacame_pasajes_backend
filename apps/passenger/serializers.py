from .models import Passenger
from rest_framework import serializers

class PassengerSerializer(serializers.ModelSerializer):
    fullName = serializers.ReadOnlyField()
    genderName = serializers.ReadOnlyField(source='get_gender_display')

    class Meta:
        model = Passenger
        fields = ('id', 'name', 'lastname', 'age', 'gender', 'fullName', 'genderName')