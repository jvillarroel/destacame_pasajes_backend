from .models import Bus, Seat, Driver
from rest_framework import serializers

class BusSerializer(serializers.ModelSerializer):
    class Meta:
        model = Bus
        fields = ('id', 'code', 'registration', 'capacity')
        read_only_fields = ('capacity',)

class DriverSerializer(serializers.ModelSerializer):
    fullName = serializers.ReadOnlyField()

    class Meta:
        model = Driver
        fields = ('id', 'name', 'lastname', 'age', 'fullName')

class SeatSerializar(serializers.ModelSerializer):
    ocuppied = serializers.SerializerMethodField()

    def get_ocuppied(self, obj):
        schedule_id = self.context['request'].GET.get('schedule', None)
        if schedule_id is None:
            return False
        else:
            return obj.is_ocuppied(schedule_id)

    class Meta:
        model = Seat
        fields = ('id', 'bus', 'number', 'ocuppied')