# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

# Register your models here.
from .models import Bus, Seat, Driver

admin.site.register(Bus)
admin.site.register(Seat)
admin.site.register(Driver)