# -*- coding: utf-8 -*-
from __future__ import unicode_literals

# Create your views here.
from .models import Bus, Driver, Seat
from rest_framework import viewsets
from .serializers import BusSerializer, DriverSerializer, SeatSerializar

class BusViewSet(viewsets.ModelViewSet):
    queryset = Bus.objects.all()
    serializer_class = BusSerializer

    def perform_create(self, serializer):
        bus = serializer.save(capacity=10)
        seats = (Seat(number=i, status='e', bus=bus) for i in range(1, 11))
        Seat.objects.bulk_create(seats)
    
class DriverViewSet(viewsets.ModelViewSet):
    queryset = Driver.objects.all()
    serializer_class = DriverSerializer

class SeatViewSet(viewsets.ModelViewSet):
    serializer_class = SeatSerializar

    def get_queryset(self):
        queryset = Seat.objects.all()
        bus = self.request.query_params.get('bus', None)
        if bus is not None:
            queryset = queryset.filter(bus=bus)
        return queryset